package at.firstproject.tobias.firstproject;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;

public class CopyrightActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.copyright);

        Intent intent = getIntent();
        int result = intent.getIntExtra("result", 0);

        TextView tvResult = (TextView) findViewById(R.id.tvResult);
        tvResult.setText("" + result);

        Log.d("TOBIAS", "Result is" + result);

    }
}
